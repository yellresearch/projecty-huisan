(function() {
  
    var extensions = {
    		onActionDeclareAsClaim: ProjectY.actions.onActionDeclareAsClaim,
    		//DT-52 Add reimburse claim
    		onActionReimburseClaim: ProjectY.actions.onActionReimburseClaim
    		//End DT-52 Add reimburse claim
    };
  
    /*
     * Now, augment the extensions into prototype
     */
    YAHOO.lang.augmentObject(Alfresco.DocumentActions.prototype, extensions, true);
  
})();