(function() {

	ProjectY = {
			
		actions: {
			
			//DT-52 Reimbursed claim
			onActionDeclareAsClaim: function onActionDeclareAsClaim(node) {
				ProjectY.actions.onActionClaim.call(this, node, "declare-as-claim", "com/yellresearch/claim/declare", "actions.projecty.declare-as-claim.title");
			},
			onActionReimburseClaim: function onActionReimburseClaim(node) {
				ProjectY.actions.onActionClaim.call(this, node, "reimburse-claim", "com/yellresearch/claim/reimburse", "actions.projecty.reimburse-claim.title");
			},
			// End DT-52 Reimbursed claim
			onActionClaim: function onActionClaim(node, formId, url, formTitle) {
				// $this will be the instance of component we merged into,
				// either Alfresco.DocumentList or Alfresco.DocumentActions
				var $this = this;
				var jsNode = node.jsNode;
				var nodeRef = jsNode.nodeRef;

				// let's create a loading message for better user experience (UX)
				var loadingMsgDialog = Alfresco.util.PopupManager.displayMessage({
					displayTime: 0, // infinite
					modal: true, // user cannot click outside
					text: "<span class='wait'>" + $this.msg("label.loading") + "</span>",
					noEscape: true // user cannot close it
				});
  
				// Read more on "Javascript Constants" section below
				var formDisplayUrl = Alfresco.constants.URL_SERVICECONTEXT + "components/form?itemKind={itemKind}&itemId={itemId}&destination={destination}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true";

				// Substitute templates with real values
				formDisplayUrl = YAHOO.lang.substitute(formDisplayUrl, {
					itemKind: "node",
					itemId: nodeRef,
					mode: "edit",
					submitType: "json",
					formId: formId // this is what we've defined previously
				});
				
				// this is our Alfresco webscript
				var formSubmitUrl = Alfresco.constants.PROXY_URI + url;
				
				// Use Alfresco provided component: SimpleDialog
				// an alfresco instance will always have an ID
				var dialog = new Alfresco.module.SimpleDialog($this.id + formId);

				dialog.setOptions({
					width: "40em",
					templateUrl: formDisplayUrl,
					actionUrl: formSubmitUrl,
					destroyOnHide: true,
					doBeforeDialogShow: {
						fn: function doBeforeDialogShow(p_form, p_dialog){
							var fileSpan = '<span class="light">' + Alfresco.util.encodeHTML(node.displayName) + '</span>';
							Alfresco.util.populateHTML([ p_dialog.id + "-form-container_h", $this.msg(formTitle, node.displayName), fileSpan ]);
							
							if (loadingMsgDialog) {
								loadingMsgDialog.destroy();
							}
						}
					},
					doBeforeFormSubmit: {
						fn: function doBeforeFormSubmit(form, obj) {
							var nodeRefElements = document.getElementsByName("nodeRef");
							var nodeRefElement = nodeRefElements[0];
							nodeRefElement.value = nodeRef;
						}
					},
					onSuccess: {
						fn: function onSuccess(response) {
							var jsonResponse = response.json;
 
							// ideally we would need to check the response, but in this case, we can assume that it's a success
							Alfresco.util.PopupManager.displayMessage({
								// an alfresco instance has this function "msg" to access I18N properties
								text: $this.msg("actions.projecty." + formId + ".success")
							});

							// refresh just the list, this will not refresh the page ;-)
							YAHOO.Bubbling.fire("metadataRefresh");
						}
					},
					onFailure: {
						fn: function onFailure(response) {

							// ideally we should display an error message from the webscript
							Alfresco.util.PopupManager.displayPrompt({
								title: $this.msg("message.failure"),
								text: $this.msg("actions.projecty." + formId + ".failure"),
								buttons: [{
							        	  	text: $this.msg("button.ok"),
							        	  	handler: function() {
							        	  		// NOTE: this (not $this) is referring to the popup instance
							        	  		this.destroy();
							        	  	}
							          	}]
							});
						}
					}
				});
				dialog.show();
			}
		}
  
	};
})()