<div class="dashlet">
    <div class="title">Claims Management</div>
    <div class="body">
        <#if foundSite>
            <a href="${url.context}/page/site/claims/dashboard">Go to claims site</a>
        <#elseif user.isAdmin>
            Please create a "claims" site.
        <#else>
            Please request site creation from your system administrator.
        </#if>
    </div>
</div>