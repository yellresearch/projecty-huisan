function testReimburse() {

	// Test script for reimburse claim
	// webscript URL
	var REIMBURSE_URL = "/alfresco/service/com/yellresearch/claim/reimburse?alf_ticket=";
	
	// variables declaration
	var PAYMENTDATE = "2013-07-30T00:00:00Z";
	var PAYER = "Employer";
	
	test("Make reimbursement", function() {
		var data = $.ajax({
			async: false, 
			url: REIMBURSE_URL + userTicket,
			type: "POST",
			data: JSON.stringify({
				nodeRef: nodeRef,
				paymentDate: paymentDate,
				payer: payer 
			}),
			dataType: "json",
			contentType: 'application/json; charset=utf-8'
		});
		var response = eval("(" + data.responseText + ")");
		ok (response, "Actual Result: " + data.responseText);
		if (response) {
			equals(response && response.success, true, RESPONSE_SUCCESS);
		}
	});
}