// constants
// URL for the webscript to be invoked
// can get it from the .desc.xml of the webscript
function testClaims () {
	var userTicket = getLoginTicket();
	var MAKE_DOCUMENT_CLAIMABLE_URL = "/alfresco/service/com/yellresearch/claim/declare?alf_ticket=";
	var RESPONSE_SUCCESS = "Expected Success value is true, returned result";
	  
	var nodeRef = "workspace://SpacesStore/00955a9c-1bef-4c24-9d36-ab47f40ac298"; // create a test document through the Alfresco web interface, and paste its nodeRef here
	var requestor = "Employee" // paste the employee user name here
	var amount = 20;
	var occurred = "2013-06-30T00:00:00Z"; // the webscript is expecting ISO8601 format
	var dueBy= "2013-07-30T00:00:00Z";
	 
	// Make document claimable
	// try to invoke the webscript with the input parameter that we prepared
	test("Make document claimable", function() {
	    var data = $.ajax({
	        async: false,
	        url: MAKE_DOCUMENT_CLAIMABLE_URL + userTicket,
	        type: "POST",
	        data: JSON.stringify({
	            nodeRef: nodeRef,
	            requestor: requestor,
	            amount: amount,
	            occurred: occurred,
	            dueBy: dueBy
	        }),
	        dataType: "json",
	        contentType: 'application/json; charset=utf-8'
	    });
	    var response = eval("("  +  data.responseText  +  ")");
	    ok(response, "Actual Result: " + data.responseText);
	    if(response){
	        equals(response.success, true, RESPONSE_SUCCESS);
	    }   
	});
}
