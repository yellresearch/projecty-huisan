//CONSTANT
var GROUP_EMPLOYERS = "GROUP_employer"; // must match with the identifier of group we created earlier

function main(node) {
	
	//DT- 54 Email notification
	//check if node is a document type, return false (do nothing) if it is not
	if (!node.isSubType("cm:content")) {
		return;
	}

	//check if node has projecty:claimable aspect, return false (do nothing) if it is not
	if (!node.hasAspect("projecty:claimable")) {
		return;
	}

	//assign the properties value in declareAsClaim to local variables
	var requestor = node.properties["projecty:requestor"];
	var amount = node.properties["projecty:amount"];
	var occurred = node.properties["projecty:occurred"];
	var dueBy = node.properties["projecty:dueBy"];

	if (!requestor) {
		return;
	}

	if (!amount || isNaN(amount)) {
		return;
	}
	
	if (!occurred) {
		return;
	}
	//End DT- 54 Email notification
	
	//if this is a valid claim, send out email to employers when a claim is declared
	//extract the properties value to local variables and form the email message to something like the following:
	var message = requestor + " has made a claim with an amount of RM " + amount + " occurred on " + occurred;
	if (dueBy != null) {
		message += ". It's due by " + dueBy;
	}


	//send mail
	// create mail action  
	var mail = actions.create("mail");  
	mail.parameters.to_many = GROUP_EMPLOYERS;  
	mail.parameters.subject = "[Yell Research] New claim from: " + requestor;  
	mail.parameters.from = "no_reply@yellresearch.com";  
	mail.parameters.text = message;  
	// execute action against a document      
	mail.execute(node);
 
}
main(document);