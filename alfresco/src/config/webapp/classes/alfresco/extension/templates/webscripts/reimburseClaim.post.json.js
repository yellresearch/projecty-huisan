var nodeRef = json.has("nodeRef") ? json.get("nodeRef"): "";
//search for the node
var node = search.findNode(nodeRef);

if (!nodeRef) {
    throw "nodeRef is undefined";
}


if (!node) {
    throw "Invalid nodeRef. Couldn't find node with: " + nodeRef;
}
  
// must be a document
if (!node.isSubType("cm:content")) {
    throw "Invalid node type: " + node.typeShort + ". Sub-type of cm:content is required";
}

// to check if the payment is made
var isPaid = true;
var paymentDate = json.has("paymentDate") ? utils.fromISO8601(json.get("paymentDate")) : null;
var payer = person.properties.userName;


if (!paymentDate) {
    throw "payment date is either undefined or is not a valid date";
}

if (!payer) {
    throw "payer is undefined";
}

// To check the properties involved
if (node.hasAspect("projecty:claimable")) {
	node.properties["projecty:isPaid"] = true;
	node.properties["projecty:paymentDate"] = paymentDate;
	node.properties["projecty:payer"] = payer;
	node.save();
} else {
		throw "This node is not a claim yet";
}

model.output = jsonUtils.toJSONString({
    success: true,
    responseCode: 200,
    responseMessage: "Document is now a reimbursed claim",
    payLoad: {}
});