var nodeRef = json.has("nodeRef") ? json.get("nodeRef") : "";
  
if (!nodeRef) {
    throw "nodeRef is undefined";
}
  
// search for the node
var node = search.findNode(nodeRef);
  
if (!node) {
    throw "Invalid nodeRef. Couldn't find node with: " + nodeRef;
}
  
// must be a document
if (!node.isSubType("cm:content")) {
    throw "Invalid node type: " + node.typeShort + ". Sub-type of cm:content is required";
}
  
var requestor = json.has("requestor") ? json.get("requestor") : "";
var amount = json.has("amount") ? parseFloat(json.get("amount")) : 0;
var occurred = json.has("occurred") ? utils.fromISO8601(json.get("occurred")) : null;
var dueBy = json.has("dueBy") ? utils.fromISO8601(json.get("dueBy")) : null;
// we'll use the default value for property "isPaid": false
  
if (!requestor) {
    throw "requestor is undefined";
}
  
if (!amount || isNaN(amount)) {
    throw "amount is either undefined or is not a number";
}
  
if (!occurred) {
    throw "occurred is either undefined or is not a valid date";
}
 
if (!dueBy) {
    throw "dueBy is either undefined or is not a valid date";
}
  
if (node.hasAspect("projecty:claimable")) {
    throw "Document is already a claim: " + nodeRef;
}

var obj = {
	    "projecty:requestor": requestor,
	    "projecty:amount": amount,
	    "projecty:occurred": occurred,
	    "projecty:dueBy": dueBy
};
node.addAspect("projecty:claimable", obj);

model.output = jsonUtils.toJSONString({
    success: true,
    responseCode: 200,
    responseMessage: "Document is now a claim",
    payLoad: {}
});